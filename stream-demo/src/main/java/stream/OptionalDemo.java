package stream;

import stream.model.Person;

import java.util.Optional;

public class OptionalDemo {
    public static void main(String[] args) {
        Person person = new Person("john@gmail.com",12,12,"nan","zhangsan");
        Person person1=null ;
        Optional<Person> opt_nullable = Optional.ofNullable(person);
        opt_nullable.ifPresent(System.out::println);
    }
}
