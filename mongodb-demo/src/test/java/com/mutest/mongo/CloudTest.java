package com.mutest.mongo;

import com.mutest.mongo.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MongoApplication.class)
public class CloudTest {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void saveTest() {
        User user = User.builder()
                .age(21)
                .id("213")
                .city("南京")
                .password("123")
                .username("132")
                .createTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))).build();
        mongoTemplate.insert(user);
    }

    @Test
    public void updateTest() {
        User user = new User();
        user.setId("213");
        user.setUsername("132");
        user.setPassword("999999");

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(user.getId()).and("username").is(user.getUsername()));
        Update update = new Update();
        update.set("password", user.getPassword());
        mongoTemplate.updateFirst(query, update, "user");
    }
    @Test
    public void upsertTest() {
        long count6 = mongoTemplate.count(new Query(), User.class);
        System.out.println(count6);
    }
}
