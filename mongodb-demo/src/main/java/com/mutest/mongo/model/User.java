package com.mutest.mongo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

/**
 * @author muguozheng
 * @version 1.0.0
 * @createTime 2022/8/30 14:21
 * @description 对应mongodb的实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document("user")
public class User {
    @MongoId
    private String id;
    private String username;
    private String password;
    private Integer age;
    private String city;
    private String createTime;
}
