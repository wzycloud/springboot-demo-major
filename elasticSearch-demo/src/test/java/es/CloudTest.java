package es;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
public class CloudTest {

    RestHighLevelClient client;

    /**
     * term精确查询
     */

    @Before
    public void doBefore() {
        System.out.println("连接");
        client = new RestHighLevelClient(RestClient.builder(new HttpHost("124.223.174.88", 9200)));
    }

    @Test
    public void queryTerm() throws IOException {
        // 根据索引创建查询请求
        SearchRequest searchRequest = new SearchRequest("person");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 构建查询语句
        searchSourceBuilder.query(QueryBuilders.termQuery("name.keyword", "张无忌"));
        System.out.println("searchSourceBuilder=====================" + searchSourceBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        List<Map> list = Arrays.stream(response.getHits().getHits()).map(e -> e.getSourceAsMap()).collect(Collectors.toList());
        System.out.println(response.getHits());
        System.out.println(JSONObject.toJSON(response));
    }

    @Test
    public void queryTerms() throws IOException {
        // 根据索引创建查询请求
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.termsQuery("sect.keyword", Arrays.asList("明教", "武当派")));
        SearchRequest source = searchRequest.source(searchSourceBuilder);
        SearchResponse response = client.search(source, RequestOptions.DEFAULT);
        System.out.println(JSONObject.toJSON(response));
    }

    @Test
    public void rangeQuery() throws IOException {
        // 根据索引创建查询请求
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.rangeQuery("age").gte(10).lte(100));
        SearchRequest source = searchRequest.source(searchSourceBuilder);
        SearchResponse response = client.search(source, RequestOptions.DEFAULT);
        System.out.println(JSONObject.toJSON(response));
    }

    @Test
    public void prefixQuery() throws IOException {
        // 根据索引创建查询请求
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.prefixQuery("sect.keyword", "朝"));
        SearchRequest source = searchRequest.source(searchSourceBuilder);
        SearchResponse response = client.search(source, RequestOptions.DEFAULT);
        System.out.println(JSONObject.toJSON(response));
    }

    @Test
    public void wildcardQuery() throws IOException {
        // 根据索引创建查询请求
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.wildcardQuery("name.keyword", "张*"));
        SearchRequest source = searchRequest.source(searchSourceBuilder);
        SearchResponse response = client.search(source, RequestOptions.DEFAULT);
        System.out.println(JSONObject.toJSON(response));
    }

    @Test
    public void boolQuery() throws IOException {
        // 根据索引创建查询请求
        SearchRequest searchRequest = new SearchRequest("person");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery()
                .filter(QueryBuilders.boolQuery()
                        .must(QueryBuilders.termQuery("sex", "女"))
                        .must(QueryBuilders.rangeQuery("age").gte(10).lte(60))
                        .mustNot(QueryBuilders.wildcardQuery("sect.keyword", "明*"))
                        .should(QueryBuilders.termQuery("address.keyword", "峨眉山"))
                        .should(QueryBuilders.termQuery("skill.keyword", "暗器"))
                        .minimumShouldMatch(1));
        searchSourceBuilder.query(boolQueryBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        System.out.println("=======================================");
        System.out.println(Arrays.toString(response.getHits().getHits()));

    }

    @Test
    public void maxQueryTest() throws IOException {
        AggregationBuilder aggBuilder = AggregationBuilders.max("max_age").field("age");
        SearchRequest searchRequest = new SearchRequest("person");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 将聚合查询条件构建到SearchSourceBuilder中
        searchSourceBuilder.aggregation(aggBuilder);
        System.out.println("searchSourceBuilder----->" + searchSourceBuilder);

        searchRequest.source(searchSourceBuilder);
        // 执行查询，获取SearchResponse
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        System.out.println(JSONObject.toJSON(response));


    }

}
